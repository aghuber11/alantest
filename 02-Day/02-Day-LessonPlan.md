## 16.2 Lesson Plan - HTML/CSS Box Model

---

## Overview

Today's class will expand students' knowledge of HTML and CSS basics.

## Learning Objectives

By the end of class today, students will:

- Use CSS to target HTML tags with IDs and classes.

- Learn to use the web inspector to experiment with CSS in the browser.

- Use height, width, margin and paddings to create containers for our USPS style guide.

- Implement Google fonts on our USPS style guide.

- Build a style guide for USPS using the concepts taught in class today.

## Class-At-A-Glance

We will discuss the box model and how it is used to display content on the web.

We will cover tags used to build the structure of the web, mainly focusing on tags that are used as containers.

Students will learn how to target IDs and classes with CSS. We will also discuss margins and paddings and how they are used to create a structure that is visually pleasing.

## Preparing For Class

- Remember, students will still be very new to coding. There will likely be many questions. Be sure to talk individually with students and make sure they are doing OK.

- Remember to enlarge the copy on your text editor so that students can see what you're doing (Command + for Mac users, ctl + for Windows)

- Today is more about the structure of the web. Be sure to explain how and when to use the div and section tags to create legible code.

- Be sure to remind students that we are just going over the basics of how HTML and CSS work. These skills are fundamental to understanding how to make pretty interfaces. We will be creating more complete webpages soon!

## Time Tracker

- Be sure to open and follow the [16-Week/03-Time Trackers/16.2 Time Tracker](https://docs.google.com/spreadsheets/d/1cR8QXVOlRoF04w_Vlm9XqyIF-Qg_8FbgzpBz5Ob9iX8/edit).

---

## 1. Instructor Do: Welcome the Class (1 min)

Welcome the class and introduce the topic of the day — HTML/CSS Box Model.

Open up the [16-Week/01-Slides/16.2 HTML & CSS Box Model Slides](https://docs.google.com/presentation/d/1HajJ71URwuHiCPEggZdk9TEEAEYXMl57dELoyWOpcWk/edit) for the day.

**Today we will be building a style guide for USPS.**

We will be applying the concepts discussed in class today to create a basic webpage for a USPS style guide. While it may seem simple, there is plenty of work to be done!

_Say to the class:_ Today we will be building a style guide for USPS.

![UPS style guide](Images/ups_styleguide.png)

## 2. Instructor Do: Introduce the Class Objectives (1 min)

Introduce the class objectives.

By the end of class today, students will:

- Use CSS to target HTML tags with IDs and classes.

- Learn to use the web inspector to experiment with CSS in the browser.

- Use height, width, margin and paddings to create containers for our USPS style guide.

- Implement Google fonts on our USPS style guide.

- Build a style guide for USPS using the concepts taught in class today.

## 3. Instructor Do: Review the Basics of HTML/CSS (5 min)

Students who are new to programming will benefit from frequent reviews of what has been covered.

Today we will start the lecture off with a review of the concepts we discussed last class.

Ask the students:

  "What does HTML stand for? How is it used?"
  "What does CSS stand for? How is it used?"
  "How do HTML and CSS work together?"

Ask the students if they want a recap of any topics covered last class.

## 4. Instructor Do: Targeting with Classes and IDs (5 min)

So now that we know how to properly structure our containers, we need to know how to style them individually.

To do this, we use two different CSS attributes, the ID and class tags.

- ID tag: ID stands for identifier. IDs are used to create unique instances of a CSS selector. IDs are *unique* to a page. This means that you should not have two IDs that are named the same.

- Class tag: Classes are identifiers that are used to target multiple elements across the same page. This means that you can reuse classes to apply the same style to all particular elements of a page.

For example, if you had two recurring elements on a page, you could use a class to style both of them exactly the same. No need to type out your CSS properties twice—just use a class.

## 5. Instructor Do: Divs and Section Tags (5 min)

For students to be able to build beautiful interfaces, they first need to understand how to create the underlying structure.

- Many of the questions you will receive will be about how to position containers. Make sure you're up to date on the different positioning properties.

- If you need to brush up on your CSS layout skills, please go through the following tutorial:
    • [Learn CSS Layout](http://learnlayout.com/)

It is very important for students to understand how to build the structure for their HTML. The base of this is the Section and Div tags.

- Section - The section tag needs no explanation; the name says it all. This tag is used to build specific sections of a webpage. Generally, these containers define the boundaries of your website and divs are positioned inside them to create content that users interact with.

- Section tags can be used to define sections in a document, such as footers, hero images, or sections that showcase features or products specifically.

- Div stands for division. It is often used to create content meant to be positioned inside a container. The div tag is used to group block and inline elements to be formatted with CSS.

- Think of a sign-in form. The form's container is probably a div positioned inside a section.

- Knowing when to use a div and a section tag helps students write semantic code — HTML that introduces meaning to the web page rather than just presentation. For example, a p tag indicates that the content within is a paragraph.

This makes your code easier to understand for yourself and your fellow developers.

## 6. Instructor Demo: Chrome's Web Inspector (5 min)

A common concern among beginners to CSS is that it's hard to work with due to the fact that you have to refresh your browser every time you want to see how the CSS you wrote displays visually.

Enter Chrome's web inspector tools. The web inspect can be used to view CSS visually and lets you play with your design.

The web inspector is a game changer. It allows you to see exactly what you are doing when you apply certain CSS properties.

In Chrome, to view the web inspect, you can do a couple of things:

- Right click on an element and click inspector.
- Use the hotkey (Mac: CMD + Option + i; Windows: Shift + ctrl + i).

Once you have the inspector open, you will see this interface:

![Web Inspector Interface](Images/webInspecter_annotated.jpg)

Walk through annotated web inspector image:
- Open webInspecter_annotated.jpg in front of the class [webInspecter_annotated.jpg Location](Images/webInspecter_annotated.jpg);
  1. This is the elements panel. It is used to go through all the HTML structure of a page.
  2. This is the console page. It is used to check for errors on your page. It will be used more when we talk about JavaScript. However, it can display errors related to improperly linked image and CSS files.
  3. This is the styles panel. It displays what styles are being inherited by your html elements.
  4. This is the Box Model panel. You can see margin and padding that is applied to your elements in a visual manner. It also shows how these properties interact with your object.
  5. This is the element.style pane. You can use it to apply inline styles to your elements. When you reload the page, however, your styles will reload back to what's in your CSS file.
  6. Add styles by clicking between the brackets { }.
  7. You can also add styles directly to selectors you write in your CSS file. Click in the brackets to modify CSS rules and you will see the effects applied to the site as if they were written in your CSS file.

Demo web inspect tool on Google homepage:

1. Open the Chrome browser.
2. Type https://www.google.com into the browser and click enter.
3. Right click on Google Search button to the bottom left of the form.
4. Click Inspect. The web inspector will open in the right corner of your browser window.
5. Click between the brackets in element.style { }.
6. Add the CSS property background: red;.
7. Add the CSS property color: white;.
   - Tell the students that the web inspector applies the CSS style you enter to whatever element you select so you can see the result.
   - Tell the class that most front-end developers create skeletons of HTML and then style them accordingly in the web inspect and copy-paste the styles that look correct.
8. In the elements panel, mouse around the different HTML elements and show students how the web inspector highlights the element you are mousing over visually in your design. Notice how the elements are outlined by boxes?
9. Tell the class that the web is literally made up of boxes. Even shapes that don't appear to be boxes are in fact boxes.

Tell the class that the elements panel can be used to locate elements in your HTML. You always have the option of right clicking and hitting inspect again to view the CSS of whatever you are inspecting.

> Note: It's important for students to play with CSS in the web inspector. It helps the students' understanding of how CSS works when they can see the results of their code visually in front of their eyes.

## 7. Student Do: Containers & Structure (40 min)

Share a copy of the activity with the class: [16-Week/02-Activities/16.2/16.2-01 Containers & Structure](https://docs.google.com/document/d/1ZHItfbDeE0CMHL2ZMY-7DDlYsjO8tikC6imisB1SfAk).

The first step to building any project is creating its content and structure. Using divs, sections, a header, and p tags, we will construct the scaffolding of our style guide.

- Note for students: During this activity, we will type out HTML structure. Some html elements use the same structure with just slightly tweaked content. Feel free to copy-paste contents that are being reused. Just make sure you modify the contents!

- If you encounter a tag you don’t know, Google for it! Any resource from W3 schools is worth reading! For example, if you don’t know what a break tag is, search for “break tag w3schools.” W3School is an excellent resource for complete beginners to HTML / CSS.

- If you don’t manage to finish the activities during the allotted time period, don't stress. The unsolved files for the next activity will have all the correct CSS / HTML you need to continue working on the activities.

## 8. Review: Div and Section Tags (5 min)

To test the students for competency, ask them the following questions about div and section tags:

- "What is a div tag used for? When should you use this tag?"
- "What is a section tag used for? When should you use this tag?"
- "How can you use div and section tags together?"

## 9. Break (15 min-Weekday)/(45 min-Saturday)

Break time! Take 15 minutes (Weekday)/ 45 minutes (Saturday) and relax.

## 10. Instructor Do: Intro to the Box Model (5 min)

The web is made up of boxes. We saw this concept earlier during the web inspector tutorial.

**Introducing the Box Model:**

Each HTML element you create in your HTML file is represented as a rectangular box, with the box's content, padding, border, and margin built up around one another like the layers of an onion. All the box's CSS calculates on the page to display the content.

- Go to any webpage and use the Chrome inspect tool.

  - Point out the Box Model.

- The Web is made of a bunch of boxes.

  - CSS treats each element in your HTML document as a box with a bunch of different properties that determine where it appears on the page.

  - Understanding this idea will help you be a much better CSS dev and a better UI designer.

Let's walk through the properties that affect the box model:

1. **Content inside your containers:** Text and images will both cause your box to expand to contain the content inside.
2. **Height/Width:** Adding height and width to any of your elements will cause it to expand depending on what properties you set.
3. **Margin:** Margin properties are used to create space *outside* elements. Margin will move your box away from other elements that surround it.
4. **Padding:** Padding properties are used to expand space *inside* the box model. This is fundamentally the opposite from margin, which creates space outside the element.

- The box model is the building block of CSS. Everything you see on the web is a rectangle.

 - Any Questions?

## 11. Student Do: Apply The Box Model (25 min)

Share a copy of the activity with the class - [16-Week/02-Activities/16.2/16.2- 02 Apply The Box Model Google Doc](https://docs.google.com/document/d/1CnrE2HlFe0CWNg8IMkZkM-rzQEb1HSPYFWXncSDO5M0/edit)

The box model is a fundamental concept of building websites. The box model defines how your containers display in your browser window.

During this activity, we will be defining the box model of our UPS style guide. We still aren’t making things pretty, but we are building the structure that we will be styling.

## 12. Everyone Do: Review — The Box Model (5 min)

To test for student competency, ask the students the following questions:

- "What is the box model?"
- "How do the height and width properties affect the box model?"
- "What is margin?"
- "What is padding?"
- "What is the difference between margin and padding?"

## 13. Student Do: Style Our Style Guide (10 min)

Slack the following instructions to students: [16-Week/02-Activities/16.2/16.2-03-Style Our Styleguide Google Doc](https://docs.google.com/document/d/1WHhEiVoBlQvp0j8iAXGQkxlZKMJz8cIfVxEAuZaGC0o).

Our style guide is now structurally sound but honestly it could use some color.

In this section, we will be adding color to our elements to breathe some life into our style guide.

## 14. Instructor Do: Review — Classes and IDs (5 min)

Let's review classes and IDs to test for student competency.

Ask the class:

- "What does the ID tag stand for? Is it used individually or can you use it multiple times?"
- "What is the class attribute used for? Is it used individually or can you use it multiple times?"
- "Why would you want to apply the same CSS properties to multiple elements?"

Ask the class if they have questions about the usage and differences between classes and IDs?

## 15. Instructor Do: Google Fonts — Adding Font Families (5 min)

As we discussed in the UI section of this course, websites look better with nice typography. The default fonts that can be used on the web aren't the best looking.

Google realized this and created [Google fonts](https://fonts.google.com/).

Google fonts allows you to host any font that is hosted on their servers on your webpage. They also have literally thousands of fonts for you to pick from that allow you to customize your webpage.

## 16. Instructor Demo: Adding Google Fonts to Your Webpage (5 min)

Let's give our students a quick demo of how easy it really is to add Google fonts to your webpage.

Google fonts is so easy to use, students can add it to their website in 3 easy steps!

![Google fonts visual walk-through](Images/google_fonts_annotated.jpg).

**Instructions.**

1. Open the solved ID and classes activity: [Classes_Ids_solved location](/Activities/04_Add-A-Custom-Font/unsolved/index.html).
2. Open https://fonts.google.com.
3. Pick a font you like by pressing the + button.
4. On the bottom right, you will see the font you selected and a link tag.
5. Copy the link tag.
6. Paste it over the custom style sheet.
7. Open index.css located in the css folder.
8. Create a css selector that targets the body tag:
```
body {

}
```
9. Add the font-family for the custom font to the body tag. Here is an example:
```
body {
  font-family: 'Open Sans', sans-serif;
}
```
10. Be sure to tell the students that they can add as many fonts as they want, but too many will slow down the website.

Easy, right?

## 17. Student Do: Custom Fonts on Your Webpage (15 min)

Slack the following instructions to students: [Custom Fonts On Your Webpage](https://docs.google.com/document/d/1afd_mkA0lpMJNm_4m4fqjGAFkJxDOilZMToHBZjl_tk)

As you know, websites look better with nice typography, which we learned about in the UI section of the course. The default fonts that can be used on the web aren't the best looking.

Google fonts allows you to host any font that is hosted on their servers on your webpage. There are literally thousands of fonts for you to pick from, allowing you to customize your webpage like a pro!

In this activity, students will add a Google font family to their page so that they can create layouts with beautiful typography.

## 18. Everyone Do: Lesson Review and Questions (10 min)

We covered a lot of content today!

Test the students' competency by asking the following questions:

- "What is the box model? Why is it important to understand the box model?"
- "Why would you want to reset your browser-specific styles?"
- "What is the web inspector and how do front-end developers use it?"
- "What is the difference between a div tag and a section tag? When should you use a div tag? When should you use a section tag?"

## 19. Instructor Do: End Class (1 min)

- Tell the class that in the next lesson, we will be learning even more CSS and HTML.

- Say good night and dismiss the class.

---

## We Want Your Feedback!

Please submit any issues or comments on the UX/UI curriculum to our Google Form.

[Submit Issues](https://docs.google.com/forms/d/e/1FAIpQLScTc104D7Fd-2fDk3E4IIwxuOe-BNhPhWffIE9VBt7_e-t3DA/viewform)

With this form you can now view the status of your submission and other issues here:
[Issue Status](https://docs.google.com/spreadsheets/d/1UyRh0f6fwtMD5SfExvk3BZxIIioicTNhXWixjmnes1c/edit?usp=sharing)

Also email mfoley@trilogyed.com and let us know how you felt today's class went.
Please include in the email subject your: University & Unit feedback e.g. UCLA - 16.2 Feedback. We'd love to hear what you think!

We will also be reaching out individually to check in and gather feedback about how your course is going.

---

## Copyright

© 2019 Trilogy Education Services, a 2U, Inc. brand. All Rights Reserved.
