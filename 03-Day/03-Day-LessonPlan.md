## 16.3 Lesson Plan - Positioning Elements On The Web

---

## Overview

Last class, students learned how to add styling to a webpage using CSS. Today's class will be about positioning elements on a webpage. Students will practice these skills in class and build a one-pager website!

After they are finished, they will be uploading these pages to GitHub and publishing their code on GitHub Pages for their friends and family to see.

Be sure to open and consult today's [TimeTracker](https://docs.google.com/spreadsheets/d/1M_NVpQ9PPpgwmjQQcJBUxeCbHIGDq007STlYZvcweFM/edit#gid=1790667838).

## Learning Objectives

By the end of class today, students will:

1. Position HTML elements with floats and margin auto.
2. Position inline, block, and inline-block level elements to build webpages.
3. Create divs that overlap by positioning elements absolutely.
4. Build their first webpage!

## Preparing For Class

- Review slide lecture: [16-Week/01-Slides/16.3-Positioning Elements on the Web Slides](https://docs.google.com/presentation/d/1MajscMk-hBwG26cowAjGFcsgZpx0cCVe3yZdWtciBFg/edit).

- **Review the Slides Beforehand**

  Review the slides before class and make any teaching notes you'll need. As you lecture, relate your own on-the-job experience whenever possible to bring what students are learning to life and connect it to their future goals.

- **Read Activity Instructions Out Loud**

  Students will be able to more readily dive into their Activities if you provide verbal guidance. There's no need to read the instructions word for word, but do summarize the key steps.

- **When Students Build their First Webpage**

  - Today is going to be the students' first time creating a whole webpage on their own. There are going to be *a lot* of questions and equally as much frustration as students begin to learn how to be front-end web developers. Reassure students that web development is hard at first—after all, the students are learning two new languages.

  - Be sure to brush up on your positioning fundamentals as this is going to account for the vast majority of questions.

  - Review [Positioning at W3C Schools](https://www.w3schools.com/Css/css_positioning.asp).

- **When Students Learn Github**

  - Git and uploading files to a cloud repository can present a difficult experience for students. Be sure to brush up on your Git skills and practice today's in-class demos before the class starts. Providing a clear and concise explanation of how these technologies work is of the utmost importance. Generally, even if the demo is sound, some students will still experience trouble uploading files to GitHub. Be prepared for Git problems.

## Time Tracker

- Have TAs consult the [16-Week/03-Time Trackers/16.3 Time Tracker](https://docs.google.com/presentation/d/1MajscMk-hBwG26cowAjGFcsgZpx0cCVe3yZdWtciBFg/edit).

---

## 1. Instructor Do: Welcome the Class (2 min)

Open the [16-Week/01-Slides/16.3-Positioning Elements on the Web Slides](https://docs.google.com/presentation/d/1MajscMk-hBwG26cowAjGFcsgZpx0cCVe3yZdWtciBFg/edit)

Welcome the class and tell students that today we will get more in depth into HTML and CSS with positioning.

Review Today's Objectives:

By the end of class today, students will:

- Position HTML elements with floats and margin auto.
- Position inline, block, and inline-block level elements to build webpages.
- Create divs that overlap by positioning elements absolutely.
- Build their first webpage!

## 2. Instructor Do: Floats & The Display Property (5 min)

📌 During this section the goal is for students to understand how to position elements on the web using floats. Student will also be introduced to the display property which will help students understand how to them build content on the web. This section supports learning objective 1 & 2.

💼 The main takeaway from this section is to understand the two uses for floats, positioning content and floating images around text and other inline elements. Most importantly students need to understand the differences between the display property and how they are used.

The float CSS property places an element on the left or right side of its container, allowing text and inline elements to wrap around it. The element is removed from the normal flow of the page.

- Float can also be used to float two items next to each other, causing it to sometimes be used to build layouts.

- Using float to build parent containers is not recommended—there are definite drawbacks to using such an approach.

When an element is applied with either float: left or float: right, its height and width will no longer be calculated with its parent containers. This can cause the parent container to collapse because it doesn't contain any content according to our browser!

Additionally, floated elements will sometimes get stuck on other elements—this can only be fixed by using the CSS property clear.

Because of these issues, we do not recommend using float for layouts. Use floats for their intended purpose which is *floating text around an image.*

**CSS Display Property.**

The display property specifies if and how an element is displayed.

Display is fundamentally the most important CSS property for controlling layouts. If students do not have a solid understanding of how these properties work, building beautiful layouts will be difficult.

Every HTML element has a default display value depending on what type of element it is. The default display value for most elements is block or inline.

There are three display properties students should know:

- Inline
- Block
- Inline-block

We will be discussing these next.

## 3. Everyone Do: Review - Floats and The Display Property (3 min)

Take a moment and ask the class if they have any questions about floats or the display property.

## 4. Instructor Do: Positioning HTML Elements (15 min)

📌 Positioning students on the web there are several tricks you can use throughout your web developer career to position your content correctly. During this section you will introduce students how to position inline, inline-block and block level elements. This section supports learning objective 2: Position inline, block, and inline-block level elements to build webpages.

💼 The main takeaway from this section is understanding how and why you use text align and margin: 0 auto; to center your content.

**Positioning Block-Level Elements.**

Let's define what properties a block element has.

- A block-level element always starts on a new line and takes up the full width available (stretches out to the left and right as far as it can).

- Some examples of block level elements are div tags, p tags, and ul tags.

This means that block elements are normally used as containers for inline elements (we'll define those next). Containers often are used to build elements like forms and then position them appropriately in containers.

**Positioning Inline Elements.**

First, let's define what properties an inline element has. An inline element does not start on a new line and only takes up as much width as necessary.

Some examples of inline elements are span tags and b tags and actual text that you write into your text editor (the stuff between the tags).

Inline elements can be positioned by using text align on its parent container.

This can also be accomplished by setting the display property of an element to inline and then manipulating it by setting text align on its parent.

**Positioning Inline-block level elements.**

display: inline-block; sets an element to Display an element as an inline-level block container. The element itself is formatted as an inline element, but you can apply height and width values.

Inline-block elements:

- Allow other elements to sit to their left and right.
- Respect top & bottom margins and padding.
- Respect height and width.

A good example of an inline-block level element is the image tag.

Inline block elements can also be used to create layouts by putting two inline-block elements next to each other and setting their width to 30% on one and 69% on the other. An obvious question to ask about this is why don't our numbers add up to 100%?

That is because inline-block level elements still display as inline, causing spaces to be recognized around them. This prevents you from creating perfectly responsive layouts using inline-block, but you can get close—and for some layouts, this approach works quite well!

**Width-Based Percentages (5 min).**

Setting pixel widths and heights is fine for a warm-up in creating elements, but the webpages of today are often responsive—they change their structure based on the width of the browser window.

This can be accomplished by setting the width of an element to 100%—block-level elements do this by default.

When you set an element to width: 100%, what you're telling your browser is: Make this element’s content area exactly equal to the explicit width of its parent.

So, if you have a parent container that’s 400px wide, a child element given a width of 100% will also be 400px wide, and will still be subject to margins, paddings, and borders—which can break your layout if these exceed 100% width.

Margins and paddings can also be given percentage-based widths—and they work in the same way as any other percentage-based layout.

## 5. Student Do: Practice With Positioning (20 min)

As we always say, the best way to get familiar with HTML / CSS is by applying it. We are going to have students try out the different properties we just introduced.

This will be a multi-step activity where students practice positioning elements with each property we discussed.

Slack the following instructions to students:

[Practice With Positioning Activity Instructions](https://drive.google.com/open?id=1vURZVFsbMbA_ivHBsyTREd-jrin5ftp7EDjcef_p4es).

## 6. Everyone Do: Review - Layout Positioning (5 min)

We went over quite a bit of content there, so let's take a couple of minutes to take questions about building layouts.

Ask the class:

- What is an inline element?
- What is a block element?
- What is an inline-block level element?
- What is a float used for?

## 7. Instructor Do: Position - Five Properties (15 min)

📌 Continuing on the theme of positioning we will now introduce students to five properties that are used to create unique layouts on the web. You will introduce students to static, relative, fixed, absolute and sticky positioning. This student applies learning objective 3: Create divs that overlap by positioning elements absolutely.

💼 The main takeaway of this section is for students to understand how to and when to use the different types of positioning. It is especially important for students to understand how to use relative and absolute positioning together to create unique layouts.

In CSS, the position property is used to create complex layouts where you need items placed in a specific area.

There are five properties that you should know:

- **Static**: We position the element according to the normal flow of the document. Top, right, bottom, left, and z-index does not affect elements that we have not positioned.
- **Relative**: We position the element as we would normally to the flow of the document, and then it is offset relative to itself based on the values of top, right, bottom, and left. Relative positioning is mainly used to position elements absolutely inside of them.
- **Fixed**: The element is removed from the normal document flow. It is positioned to the viewport of the browser and will always stay where you put it even if the user scrolls down the page. An example of a fixed element is a contact us button that follows you as you scroll.
- **Absolute**: This is the hardest position property to master. Absolute elements are removed from the document flow and are in position relative to their nearest-positioned ancestor. If a parent container has any position property applied to it, the absolute will be positioned inside of it. If no element has a stated position, then it will be positioned to the browser viewport or window. An example of an absolutely positioned element is one that breaks out of its bounding box without affecting content around it. Think of a profile picture for LinkedIn or Facebook.
- **Sticky**: This is the newest position property. The element is positioned according to the normal flow of the document, and then offset relative to its nearest scrolling ancestor and containing block. An example of a sticky element is a nav bar that scrolls with you when you hit a certain height.

The best way to learn about the different positioning properties is to practice with them and see what they do!

## 8. Student Do: Positioning With Relative, Absolute, and Fixed Positioning (20 min)

In this activity, students are going to be testing out three properties of Relative, Absolute, and Fixed.

Students will see how these properties work with a series of examples.

Ask TAs to Slack out the [16-Week/01-Activities/16.3/16.3-01-Positioning With Relative, Absolute, and Fixed Positioning Activity Instructions](https://drive.google.com/open?id=1n-N9mo917U0CKkMYJfcDQr8vAx8xFyVYrgho5madqbA) and review them with students.

## 9. Everyone Do: Review - Questions about Positioning? (5 min)

Ask the class if they have any questions about how to position elements in their design.

- Ask the class to share their files with you so you can help them directly on the projector.
- Take two or three questions.
- Tell students to talk to you during office hours if they have any more positioning questions.

## 10. Break (15 min-Weekday)/(45 min-Saturday)

Break time! Take 15 minutes (Weekday)/ 45 minutes (Saturday) and relax.

## 11. Student Do: Mini Project - Let's Build a 1-page Website! (50 min)

📌 In the class there are many moments for students to practice the concepts that they learned earlier. By attempting to apply the skills lectured about mini project are used to seal the concepts into the mind of students through application. Remember that  applying skills is the most important step for novice web designers. This section supports learning objective 4: Build their first webpage!

💼 It is important for students to apply their skills they have learned in class to help seal in the knowledge and concepts lectured about previously.

We are going to practice building a basic webpage that we will then upload to Github pages.

**DEMO** the solution.

- Here is a codepen showcasing the solution file: [Code solved 1-page webpage scaffolding](https://codepen.io/TrilogyEdu/pen/LwzRKz).

Slack students the following instructions:
[Build a 1-page website](https://drive.google.com/open?id=1kB8nXPMXOKEyZrtZpd6r-clzCCIBaznZ5_cW9MR6YI0).

## 12. Instructor Do: What is Git/GitHub? (10 min)

📌 Github is a version control system used to manage web projects with version control. Students will also be introduced to their first git work flow: uploading a site to Github and publishing it.

💼 The main takeaway from this section is for students to understand what version control is. It is equally important that students learn their first git workflow - publishing their code to the cloud.

The goal of this section is to justify the use of Git to designers who up to this point may not see a lot of value in using developer tools. Git is not easy and will take lots of practice. You will be helping students with Git for the rest of the Boot Camp. If you are not familiar with Git, it will be key to practice yourself, as you and the TAs will be helping troubleshoot.

- GitHub is software that is used for version control. It is free and open source.

- Why Git?

  - A version control system that allows team members (developers, UX, UI) to collaborate without overriding each other’s work.

- Git is not easy and has lot of developer jargon, but we will spend some time getting used to it!

- TAs slack out resources:

  - https://www.atlassian.com/git/tutorials/why-git
  - https://medium.com/shyp-design/managing-style-guides-at-shyp-c217116c8126
  - https://www.c-sharpcorner.com/article/what-is-version-control-git-vs-tfs/
  - https://git-scm.com/book/en/v2/Git-Internals-Git-References

- Now, let’s understand what version control is.

  - Version Control is the management of changes to documents, computer programs, large websites, and other collections of information.

  - Version control saves all the previous changes you make to a file in "commits." Each commit has a unique number that you can use to see how your project has changed over time.

  - This means that projects that are tracked by Git can be rolled back to previous versions—this is especially useful if you post faulty code up to your production server and need to roll it back.

- Different versions are stored locally on your computer in a hidden folder named .git—you won't be able to see this folder unless you have certain permissions set on your computer. I don't recommend you change those settings unless you know what you're doing.

  - So, if the changes are stored locally, how do teams use this tool to collaborate?

  - Enter cloud repositories like GitHub. GitHub is a service that provides a space for developers (and other professionals) to store their projects.

- Using Git, we can push our code up to GitHub's servers where other people can pull your code to their local machine and work with it.

- GitHub is also a valuable skill to have as a UX/UI designer and can be the difference between candidates for open roles.

We have spent all this time creating nice layouts. It would be a shame if we didn't show off a little bit.

- The students need a way to host their code—but we don't need to buy server space to accomplish this. GitHub lets you publish your HTML pages to a public URL that anyone can view: GitHub Pages.

- GitHub Pages is a static site-hosting service designed to host your personal, organization, or project pages directly from a GitHub repository.

- You can host as many pages as you want. We will be hosting all of our in-class work and homework here so that employers, friends, and family can view them.

## 13. Instructor Demo: GitHub Pages (3 min)

**Instructions:**

1. Go to https://www.github.com

   - Create a new repository.

   ![Step 1](Images/1.png)
   ![Step 2](Images/2.png)

2. Make sure you set your repo to public.
3. Create your repo.

   ![Step 3](Images/3.png)

4. Search for your repo in GitHub Desktops UI! Remember what you named it?
5. Clone your repo to your local machine.

   ![Step 4](Images/4.png)

6. Choose where you want to save the repository in your local directory. Make sure you choose a spot you are going to remember. Then click clone.

   ![Step 5](Images/5.png)

7. Drag the files you want to upload to the cloud into the folder you cloned. Make sure your root file is named index.html or GitHub won't load your page.

   ![Step 6](Images/6.png)

8. GitHub notices that you have changed the files in the repo and is now tracking it.

9. Enter a description of what is in this commit. Verbose descriptions help you remember what you pushed up each commit and helps to backtrack if needed. Only the summary is required. The description is optional.

10. After you have entered your description, click commit to master.

## 14. Student Do: Upload Your First Webpage to the Cloud! (20 min)

Understanding how to push code is important for students—many companies are beginning to use Git to manage more than just code projects.

Slack students the [Upload Your First Webpage to the Cloud! Activity Instructions](https://drive.google.com/open?id=1MzsC-6BrQhgYnF6C6lU_AUwYR5RarfETL_jXucoEcP0).

Remind students that GitHub can sometimes take up to 15 minutes to make the changes.

## 15. Recap and End Class (1 min)

Recap what the students learned in class today.

Say good night and dismiss the class.

---

## We Want Your Feedback!

Please submit any issues or comments on the UX/UI curriculum to our Google Form.

[Submit Issues](https://docs.google.com/forms/d/e/1FAIpQLScTc104D7Fd-2fDk3E4IIwxuOe-BNhPhWffIE9VBt7_e-t3DA/viewform)

With this form you can now view the status of your submission and other issues here:
[Issue Status](https://docs.google.com/spreadsheets/d/1UyRh0f6fwtMD5SfExvk3BZxIIioicTNhXWixjmnes1c/edit?usp=sharing)

Also email mfoley@trilogyed.com and let us know how you felt today's class went.
Please include in the email subject your: University & Unit feedback e.g. UCLA - 16.3 Feedback. We'd love to hear what you think!

We will also be reaching out individually to check in and gather feedback about how your course is going.


---

## Copyright

© 2019 Trilogy Education Services, a 2U, Inc. brand. All Rights Reserved.
