## 16.1 Lesson Plan — Introduction to HTML/CSS

------

## Overview

Welcome to the wide world of programming! Today's class will introduce students to the basics of HTML/CSS. By the end of the day, students will create a basic webpage in HTML/CSS.

- How this lesson fits into the next five weeks.

For the next five weeks, students will learn various aspects of HTML, CSS & JavaScript. During these five weeks, students will work to build their design portfolio.

**PLEASE NOTE:**

During activities in the code section, students will need the working files for each day. They all live on the UX/UI repository under the activity folder for each day.

Make sure you take a pull from the repository before class so you can share the working files with students.

Students will use these working files in tandem with Google Docs instructions to complete activities.

## Learning Objectives

By the end of class today, students will:

- Install development tools (Visual Studio Code, Github Desktop) that will be used during this class and in the work environment.

- Create a project folder structure with 1 main folder to hold all their projects that will serve as a workspace for students using VsCode & the operating system. This sets the students up for success later down the road when projects get more complex and numerous.

- Learn and practice writing HTML tags that are used to build structure and display content on the web.

- Code a link tag to link a CSS document with an HTML document.

- Learn to select basic HTML tags in CSS and style these elements' color, type, and background using CSS.

------

## Preparing For Class

- Review slides for today's lesson: [16-Week/01-Slides/16.1 Intro to HTML & CSS Slides](https://docs.google.com/presentation/d/12tno3IZCgCoqfClz5-vLKE5MgxeASBD28EWpivMXudg/edit#slide=id.g4068525ce9_2_24)

- **Career Services Slides**

  We want to make students aware of the services available to them to help guide their career transition. Please spend 10-15 minutes at the beginning of lecture covering these slides. Due to this announcement, class may run a little longer than usual.

  - [Career Services Slides](https://docs.google.com/presentation/d/18nx5sCgXLK_jKjx7MDbJE7ZINAjczFZN-KEVvZCPSZI/edit?usp=sharing)

- **Review the Slides Beforehand**

  Review the slides before class and make any teaching notes you'll need. As you lecture, relate your own on-the-job experience whenever possible to bring what students are learning to life and connect it to their future goals.

- **Download the following tools on your computer**

  - [VS Code](https://code.visualstudio.com/)
  - [Github Desktop](https://desktop.github.com/)

- **Prepare for Today's Demos**

  - There are plenty of demos that showcase basic things like how to use a text editor and how to write css selectors. Be sure to read through the LP and practice the demos before the class!

- **Helping Student's Program**

  - Many students are going to be nervous about getting into programming. Some may even have impostor syndrome. Reassure students who are feeling this way that it's normal and will pass. After all, they are learning a new language.

  - Be sure to mention that the best way to feel more comfortable working with code is to code! Every coder working today had to start coding at some point. Your students are no different from them. Practice makes perfect; the best way to get rid of impostor syndrome is to practice.

  - Don't be afraid to talk about the difficulties you experienced when you first started learning how to program. This will help students feel better about not completely understanding the material yet.

- **Coding Requires a Different Setup**

  - During the coding portion of lecture, it is very common for instructors to mirror their laptop screen for the projector.

  - Various instructors have used different methods to help them facilitate coding, such as bringing an additional laptop, iPad or phone to review the materials during lecture.

**Recommended VsCode Settings.**

- When displaying code to the class, it's important for the text to be legible. Make sure you enlarge the text on your text editor so the students can see. In Visual Studio Code, the best way to accomplish this is with cmd and + for Mac and shift and + for Windows. For reducing the size, the hotkeys are cmd and - (iOS) or Shift and - (Windows).

  1. Set font size to 24 pixels! This allows students to see your code well even if they're in the back. This setting is under Code > Preferences > Settings > Commonly Used > Editor: Font size
  ![Font Size VsCode](Images/font_size.png)

  2. It is recommended that you change to a white-colored theme, as most of our boards are white. You can find these settings under Code > Preferences > Settings > Search for Theme > WorkBench: Color Theme
  ![VsCode Themes](Images/VsCode_Themes.png)

## Time Tracker

- Keep track of the clock. Have TAs consult the [16-Week/03-Time Trackers/16.1 Time Tracker](https://docs.google.com/spreadsheets/d/16KnDbICkNf9zOzxRcTU_j1jktSrwmQc88FKrmyIoX5s/edit?usp=sharing).

---

## 1. Instructor Do: Career Services Presentation (15 min)

Open these slides and review with students: [Career Services Slides](https://docs.google.com/presentation/d/18nx5sCgXLK_jKjx7MDbJE7ZINAjczFZN-KEVvZCPSZI/edit?usp=sharing).

- As we near the end of the course, we should talk about Career Services. Spend the next 15 minutes discussing the support available to students. By the end of this conversation, students will have a clearer understanding of the next steps they should take in their job searches.

- Open Slides [16-Week/01-Slides/16.1 Intro to HTML & CSS Slides](https://docs.google.com/presentation/d/12tno3IZCgCoqfClz5-vLKE5MgxeASBD28EWpivMXudg/edit#slide=id.g4068525ce9_2_24)

## 2. Instructor Do: Introduce Class Objectives (5 min)

To reiterate, the class objectives for today are:

- Install development tools (Visual Studio Code, Github Desktop) that will be used during this class and in the work environment.
- Create a project folder structure with 1 main folder to hold all their projects that will serve as a workspace for students using VsCode & the operating system. This sets the students up for success later down the road when projects get more complex and numerous.
- Learn and practice writing HTML tags that are used to build structure and display content on the web.
- Code a link tag to link a CSS document with an HTML document.
- Learn to select basic HTML tags in CSS and style these elements' color, type, and background using CSS.

**Welcome to User Interface Design Units.**

Let's review what we learned in the last few weeks of the course and how it relates to what we are learning now.

**Weeks 1-7: User Research, Design Thinking, and Prototyping.**

During weeks 1-7, we learned about the value of design thinking and how UX designers approach solving problems.

We learned how UX professionals research their users to gather insights and evidence that support their digital idea, product, or service that they are designing.

We learned how to create prototypes using our insights into our users that showcase our new or redesigned features in a clear and straightforward way.

We created a wireframe of a Mobile Travel App prototype by first researching our assumptions about our users and then using their insights to create a product that is both useful and functional.

**Weeks 8-15: Visual Design and Animation.**

During weeks 8-15, we learned how to build and add visual design to our prototypes.

We crafted sleek designs by applying Gestalt principles of visual design and learned how to design on a grid to create a fluid responsive layout.

We interviewed users before we tested our assumptions about the viability of our product or idea. We then tested our designs by continuing to prototype and test our full-color prototype iterations.

To cap off this section, we applied everything we learned in all 15 weeks to a responsive UI redesign of a government agency website. We also created assets to support our design decisions, such as a style guide and a wireframe that utilized a full-color responsive design and insights gleaned from user research.

**Weeks 16-21: Front End Development.**

This of course leads us to the Front End Development portion of this curriculum.

During this part of the course, we will focus on how to bring our designs to life on the web.

We will learn how to build UI elements and layouts by focusing on the basics of HTML and CSS structure.

We will add animations and interactivity to our website using jQuery to breathe life into our user interfaces.

We will learn how to create webpages in a visual format using Webflow. We will analyze our digital products by using Google Analytics & Hotjar to track our users' interactions with websites.

During the last week of the web development course, we will create our own digital portfolio and publish it on the web for all to see.

After the web portion is finished, we will conclude this bootcamp with a project that encompasses all three aspects of the course:

1. Research, Testing and Prototyping.
2. Designing full-color responsive designs.
3. Coding our work and presenting our findings in a case study.

**Students will be expected to work.**

Welcome to your new job!

During the web development section of the course, you **MUST** put in time outside of class.

We lecture about the basics and fundamentals you need to create a layout on the web, but YOU must apply the concepts to your homework for you to fully grasp the concepts.

**Expectation Setting For The Code Section.**

It is normal for developers to research how to solve problems. Developers all have one common skill—problem solving.

The best developers are expert problem solvers, but even they have to take a step back from their work to research a solution.

This applies doubly for students. Researching how to accomplish your vision of your design is essential to bringing your design to life. This is a normal process that all developers go through.

**Researching problems is normal.**

Problems vary from student to student—it is rare that two students will have the same problem. This is also true in the working world.

Instructors may not immediately know how to solve a problem depending on what it is. You may need time to find a solution for a specific problem and illustrating your process for problem solving can be just as valuable as the solution itself.

**The Path Of Learning.**

There are three obstacles when it comes to learning front end development:

1. The Great Confusion - When learning front end development (or any programming language), you will often find yourself with code that works but you don't fully understand why or code that doesn't work and you don't know why. This is normal and part of the learning process.
2. The Great Doubt - Doubt during the development section is so common there is actually a term for it. Imposter syndrome is common when you doubt you have the skills to learn web development (or anything really). The best way to relieve yourself of this feeling is to code!
3. The Great Distance - Learning to be a developer takes a long time. Writing code is actually a lifelong endeavor. Developers never stop learning and are continually adding to what they know—software changes often and you need to continue learning in order to stay relevant!

**Pair Coding.**

Encourage students to work together on problems. Trying to code alone is often a frustrating experience.

Join someone else to help you solve problems—it can be less tedious and downright fun when you figure it out together!

Sometimes companies assign programmers to "pair program" as a group.

Pair programming is an agile software development technique in which two programmers work together at one workstation. One, the driver, writes code while the other, the observer or navigator, reviews each line of code as it is typed in. The two programmers switch roles frequently.

_You can encourage students to program in pairs_.

- If you do encourage pair programming in your class, try to pair weaker students with stronger ones.

## 3. Instructor Do: What Is Front-End Development? (10 min)

Ask: "What do you think a front-end developer does?"
_Answer: A front end developer codes the html elements that you see_.

Let 2 or 3 students answer in order to get a wide variety of opinions. This will generate discussion and help students feel comfortable participating.

Front-end development is:

- The practice of building the client-facing section of webpages and user interfaces.
- Front-end developers build the parts of the web that users see and interact with.
- Front-end developers use HTML, CSS, and JavaScript to build rich interfaces that are pleasing to the eye.

**Why Do I Need To Know Front-End Development?**

Why do I need to know how to build the front end of a website? Don't they have people hired to program it for me? I just want to be a UX designer.

Believe it or not, this is a common sentiment among people new to the UX community.

But the question remains — why does a UX designer need to know how to code?

Students should learn front-end development because:

- Understanding the medium you are designing for helps you create better user interfaces. How can you make a realistic design if you don't know what is realistically possible with the programming languages used?
- Understanding the medium gives you insight into where you can push boundaries to make something cool and innovative.
- As a UX designer, you _will_ be working with developers to implement your design. Being able to communicate your ideas effectively to developers is essential.
- You need to know how to code so you can recommend and design ideas based on feasible solutions.
- Many UX Jobs require some development experience for the more senior-level roles.

_Share your experience with the class!_
Discuss with the class how having an understanding of front-end development has helped you as a UX designer.

**Do Designers Need to Know How to Code?**

Short answer: **Yes.**

The majority of user experience job descriptions have 3 things in them that makes knowing front-end development invaluable.

1. Understanding of the technical implications of UX/UI decisions.

2. Ability to communicate clearly and effectively with members of the development team and business stakeholders (this could be your boss or the owner).

3. Working knowledge of HTML, CSS, and JavaScript.

   Don't believe me? See for yourself.

   ![UX Job description 1](Images/UX_Job_description.png)

Let's run through the checklist:

1. Understanding of the technical implications of UX/UI decisions. **Check**.

2. Ability to communicate clearly and effectively with members of the development team and business stakeholders (this could be your boss). **Check**.

3. Working knowledge of HTML, CSS, and JavaScript. **Check**.

   And one more just for fun.

   ![UX Job description 1](Images/ux_job_description2.png)

## 4. Everyone Do: Questions? (3 min)

Ask the class:
"Does anyone have any questions about why, as UX designers, we are learning code?"
"Does anyone have any questions about the software we just installed?"

**Common questions students may ask:**

**How much code do UX/UI designers need to know?**
**Answer:** At a base level, you need to understand what is possible to do in your design. As creatives, it is very easy to design something that would either require too much development time because it's never been done or is not technically feasible with web technologies.

**How much code does a UX/UI developer need to know?**
**Answer:** At a base level, UI developers need to be a functional front-end developer. You need to be able to make beautiful responsive layouts as well as be able to do solid UI design work.

**Do I need to know how to program mobile apps?**
**Answer:** No. Mobile app developers are usually software engineers that specialize in app languages for the platform they develop on. That being said, the technologies used in the mobile app—some make use of HTML, CSS, and JavaScript—may make learning the skills we talk about in this class relevant.

## 5. Student Do: Software Check (5 min)

Before we dive into learning about HTML, we need to make sure we have the right tools for the job.

Ask TAs to Slack out the activity file: [16-Week/02-Activities/16.1/16.1 Software Check Activity Instructions Google Doc](https://drive.google.com/open?id=1DuDtr5x7aNJxMvsnPoiqAEqcIlC6vvwofoRN1oCzEOY).

The tools students are installing today are used by professionals and companies worldwide. If you code in any way, shape, or form, you will encounter GitHub and Visual Studio Code.

## 6. Instructor Do: Diving Into HTML (5 min)

The goal is to introduce HTML. Some students will be familiar while other will be brand new. Make sure to engage students in conversation and remind them that they will learning lots along the way, so they should try not to get overwhelmed.

**Ask:** "Does anyone know what HTML stands for?"

- HTML stands for hypertext markup language. It is the skeleton of the web.

- HTML is the structure of the web; it makes up the underlying components that are styled or animated with CSS and JavaScript to make up what you see visually.

- Having a base understanding of HTML is the very first step to being a successful UI developer. The concepts we talk about today are the foundations of how to build the web. Understanding the basic fundamentals of HTML will answer most layout questions that students have.

- HTML files are all about structure and content. The web you see is really made up of a series of tags that are then filled with content.

So we know what an HTML file is now but how do we create one? You will be showing students in the next section how to create HTML files and demonstrating the basics of working with their text editor.

**Instructor Demo: The Anatomy Of A Webpage.**

Give students a short demonstration on how to work with HTML files in your text editor and your browser.

**Instructions:**

- Create a folder in your documents or desktop named code. You will be saving all your work in separate folders here.
- For any code project you create, you will need to create another folder inside the code folder that will hold your HTML / CSS / JavaScript files. For example, for homework 16, you should just name that folder homework_16 and then save your files inside.
  ![Folder Structure](Images/code_folder.png)

- We do this for two reasons. The first is so you can stay organized. Knowing where the appropriate folders are really helps you manage your work and not lose files. The second is so that we can manage each folder separately with version control (Git/Github), with each folder serving as your local repository.

- Open Visual Studio Code.

- Click File > Add folder to workspace and select your code folder that you just created. Then click the add button.
  ![Visual Studio Folder](Images/visual_studio_folder2.png)

- Now you have a basic workspace set up that should contain an empty folder named code. It should look like the following:
  ![Visual Studio Workspace](Images/visual_studio_workspace2.png)

- Create a folder inside the code folder by right clicking on the code folder. Name it My_First_HTML.

  ![vsc_create_file_folder](Images/VSC_create_file_folder.jpg)

- Create an html file inside your new folder by right clicking it and clicking new file. Name it index.html

  ![workspace_complete](Images/myfirsthtmlComplete.jpg)

- After you show students how to create folders and files, move on to the next slide (slide 32) and walk students through the tags and folder structure.

**Tags to discuss.**

_!DOCTYPE html_ - The doctype tag defines the document to be an HTML page to the internet browser.
_The HTML tag_ - The HTML is the root element of an HTML page. It wraps all other tags except the !DOCTYPE html tag.
_head tag_ - Head tags define the head of a document. Head tags contain meta data—meta data contains additional information about your document. Here are some common tags that are contained inside our document:
  - _title tag:_ The title tag specifies the title of our webpage. It is the text you see displayed in the tabs of your internet browser.
  - _link tag:_ Link tags are used to import css files into our:
  - _body tag:_ Body tags wrap all elements that are used to define the structure of your HTML document. In other words, it contains tags that are used to display page content.
  - _h1 tags:_ h1 stands for header 1. Headers are used to introduce bodies of text and catch users' attention. Headers range from h1 to h6, with h1 being the largest and h6 being the smallest.
  - _p tags:_ P tags are used to define paragraphs on a webpage. You can, however, just write text between containing elements such as div tags.
  - _a tag:_ Anchor tags are used to tag you to other places on the web. They can reference local files and URLs on the web.

When you have finished discussing these tags, move on to slide 33:

**The Anatomy of a Webpage — continued.**

There are two important concepts to discuss with the class here:

1. Anything wrapped between two tags is known as a parent child relationship. This simple means that one tag’s opening and closing tags wrap another html element. This becomes important later when building website structure.

_Note: see below for an example of a parent child relationship_

```html
<section class="parent"><!-- I'm a parent element because my closing tags wrap other tags -->
  <div class="child"></div> <!-- I'm a child element. -->
  <div class="child"></div> <!-- I'm a child element. -->
  <div class="child"></div> <!-- I'm a child element. -->
</section>
```

2. When tags are wrapped inside another tag, they should be indented in your text editor. We do this so our code is legible and it creates a visual structure that shows what tags are wrapped in each other.

**Text Editors.**

Text editors are programs that are installed on operating systems that allow you to edit files that are composed of text. All programming languages are different variations of text files.

There are many different flavors of text editors and all perform the same basic function. In this class, we will be using Visual Studio Code.

## 7. Instructor Do: Anatomy of an HTML Element: (5 min)

Now that we know how to work with an HTML document as well as the basics of HTML tags, let's explore the syntax further.

- HTML elements consist of a start tag and an end tag, with the content inserted between the 2 tags.

- Direct the class's attention to the image below.

![Opening And Closing Tags](Images/grumpy-cat-small.png)

Notice how the text "My cat is very grumpy" is between the 2 tags?

- If you have time, pull the code back up from Anatomy of a Webpage.

- Ask the class to identify where the closing tags are in this document.

- Failing to close tags will cause the rest of your HTML to behave in a strange way. If you notice that things are not displaying as you think they should, go back and check your tags and make sure everything is closed!

## 8. Students Do: Hello World! (10 min)

Ask TAs to Slack out the activity file: [16-Week/02-Activities/16.1/16.1 02 Hello, World! Activity Google Doc](https://docs.google.com/document/d/1pW1BrZU4G0xiPuPujaohCnQo4ZYf_yvnsG11pgTnpwI/edit).

This activity is designed to reinforce student understanding of HTML and text editor basics. The goal of this activity is for students to practice using their text editor as well as to type out the basic HTML structure.

- Let the class know that it's their turn to try out the concepts we just talked about.

- Be sure to leave up the example that you created during Anatomy of a Webpage for students to use as reference.

- During the activity, walk around and answer questions from students. Ask students who appear to be struggling if you can provide clarity on any subjects that they might be confused about.

## 9. Break (10 min-Weekday)/(45 min-Saturday)

Break time! Take 10 minutes (Weekday)/ 45 minutes (Saturday) and relax.

## 10. Instructor Do: More HTML Elements! (5 min)

We went over a lot of information so far. Let's recap to make sure students understand the basics of HTML.

Ask your students the following questions to test for competency. Be sure to call on students if your class is shy.

Take 1 or 2 answers for each question if you have lots of hands in the air.

- "What is HTML and what purpose does it serve on the web?"
- "How do you preview an HTML page in your browser?"
- "How do you close an HTML tag?"
- "What is the body tag used for?"
- "What is a comment and why is it used?"

**HTML Elements.
**

During this section we will introduce some of the basic building blocks that students will use to build content on the web.

**Introduce the A tag to students:**

The a tag defines a hyperlink, which is used to link from one page to another.

The most important attribute of the <a> element is the href attribute, which indicates the link's destination.

By default, links will appear as follows in all browsers:

- An unvisited link is underlined and blue
- A visited link is underlined and purple
- An active link is underlined and red

a tags can take you to other URLs on the web. The a tag below will take you to https://www.google.com

```html
<a href="https://www.google.com">This link takes you to google.com!</a>
```

a tags can also link to other pages in your folder structure!

```html
<a href="aboutMe.html">About Me</a>
```

**Introduce the image tag to students:**

- img tag: The img tag does exactly what it sounds like it does. It loads images into your HTML file for rendering on your page.

Notice anything different? Where's the closing tag?

Spoiler alert! There isn't one. That's because an img tag is known as an empty element.

- Empty elements don't have any contents inside them like a p or h1 tag.

- Empty elements don't have a closing tag.

  - Instead, empty elements have "attributes" that tell the empty element what content it is loading.

  - In the provided example, the attribute "src" stands for source, and it's looking for a file named smile.gif in the same folder as the html page that you put the img tag in.

  - But what if your image is in a separate folder—like an images folder?

- Explain that in order for your image to display correctly if it's in a separate folder, you have to specify that in your src attribute.

    ```html
    <img src="images/your_image_here.png">
    ```

- This piece of code is looking for an image named your_image_here.png in a folder named images

  - Create an images folder and place an image in it.

  - Explain to students that you place image files in their own folder to keep your project neat and organized. Large projects tend to get cluttered very quickly if you can't keep track of where everything is.

Now that we have learned about img and a tag it's time to introduce list tags to our students:

## 11. Instructor Do: Lists, Lists, and More Lists (5 min)

Introduce the following UL, OL, and LI tags to the class.

1. ol tag: This is known as an ordered list tag. The list item in this list will be numbered!
2. ul tag: This is an unordered list tag. It will appear with bullet points.
3. li tag: This is a list item tag. It is used to contain content that appears as part of the list in ol or ul elements.

- Open Lists.html [Lists.html Location](Activities/Lists_example/Lists.html).
- Display the piece of code to the class.
- Explain the difference between ul and ol elements:
- Explain what a list item is and how it is used. Be sure to mention that list items can contain more than just text.
- Encourage students to read the documentation about lists.

We have talked about many different tags for building content on the web. It's time for students to apply the principles we have talked about by customizing a page about their favorite musician.

## 12. Student Do: My Favorite Musician (20 min)

The goal of this activity is for students to continue to familiarize themselves with the basics of HTML tags by changing the file Fav_Musician.html to be about their favorite musician.

Ask TAs to Slack out the activity file: [16-Week/02-Activities/16.1/16.1-03-My Favorite Musician Activity Google Doc](https://docs.google.com/document/d/1CM8fd-bHqgrioh7SdQXlHcIcJA4OA_xjJ6eIbhiplbw/edit).

Be sure to encourage students to read the documentation provided in Fav_Musician.html if they get lost (lines 44 - 50).

_Let the class know that they will be reusing this file for the rest of the class._

## 13. Instructor Do: Review Images and Lists (5 min)

Let's help our students remember all these tags by asking them the following questions:

- "Why does an img tag not have a closing img tag?"
- "What is the difference between an ol and a ul element?"
- "Why should we put images in their own folder?"
- "What tag can we use to display data in lists?"

## 14. Instructor Do: Introduction to CSS (5 min)

We just covered HTML quickly but remind students that HTML on its own is not that useful. So let talk how to stylize HTML with CSS.

Say: "Raise your hand if you are familiar with CSS."

  - For those of you already familiar with CSS, this will be a bit of a review but there are always new things to pick up and you can help your fellow classmates.

- CSS stands for cascading style sheets. CSS on the web is used to control how HTML elements display visually.

- HTML + CSS allows you to build awesome stuff.

  - TA slack out: https://onix.design/

  - https://www.awwwards.com/websites/css3/

  - Go to the [onix homepage](https://onix.design/) and scroll down, revealing the simple animation. This was achieved through CSS.

- Let's use the analogy of a body: HTML is the skeleton for structure and strength, whereas CSS is appearance—the skin, the hair, even clothes. CSS determines the look and feel.

- You can make a fully functional, beautiful web page with only basic HTML and CSS.

## 15. Instructor Demo: CSS Basics - Link Tags and Empty Elements Continued (5 min)

**Say to the class:** So we know what CSS is used for, but how do we actually use it in our HTML?

First, let's take a step back and reiterate what an empty element is to the class. We will be using another empty element shortly.

- An empty HTML element does not contain any content between an opening and a closing bracket. Instead it contains attributes that tell the empty element what information it is supposed to display.

- The 2 most commonly seen examples of empty elements are link tags and img tags.

- The HTML link tag is used for defining a link to an external resource. Link tags are used to load style sheet files into HTML. It is best practice to include the link tag in the document head.

Link tags are an excellent example of an empty element.

- Open the following link in the browser and Slack it out for students to read about empty elements: [Empty Elements List](https://developer.mozilla.org/en-US/docs/Glossary/Empty_element).

- Briefly mention that link and img tags are the most common empty elements.

- Be sure to click the link tag to proceed to the documentation. Tell the students to refer to this page if they get confused about the link tag: [Mozilla Link Documentation](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link).

- Display the following piece of code:

```html
<link href="style.css" type="text/css" rel="stylesheet">
```

- Explain that the rel="stylesheet" and href="style.css" are attributes for the link tag and explain what they do.
  - 'rel="stylesheet"' is telling our link tag that the document we are importing is a stylesheet.
  - 'href="style.css"' is telling our link tag that we are loading a file named style.css into our HTML document.
- Slack the link snippet to the class for easy copying and pasting.
- Open your Fav_Musician.html file  [Fav_Musician.html Location](Activities/My_Favorite_Musician/Fav_Musician.html).
- Add the add the link provided above between the head tags of the document.
- Explain that it is best practice to include link tags in the head of your document because this loads your styles before your html. If you load your styles second, it will force the browser to "repaint" your website, causing performance delays.
- Tell your students to leave this file open. We will be using it again shortly.

## 16. Instructor Do: CSS Syntax (5 min)

Finally, we get to introduce CSS syntax so we can make our websites pretty!

- Make sure students understand that CSS and HTML syntax are completely different animals, but both must be practiced.

- Call the class's attention to the following image:

![CSS syntax basics](Images/css_basics.png)

Let's break this image down.

1. Selector: The selector is used to target HTML elements as well as IDs and classes (more on this next class). The target defines what HTML elements receive which CSS properties.
2. Declaration start: The CSS properties that are applied to the selector and always typed out between 2 curly braces. The first curly brace is the declaration start.
3. Property: The CSS property being used. In this case, its background color. The property and values are always separated by a colon (:). This is known as a separator.
4. Value: The value of the property you gave to the element. In this case, we gave anything wrapped in an a tag the background color of yellow.

```
- [Reference of all CSS properties](https://www.w3schools.com/cssref/)
```

5. Declaration End: To end a CSS statement, you must have a second } enclosing the properties and values. The last curly brace is called the declaration end.

>Note: Make sure that students understand that CSS values are unique to their property. Be sure to tell students to bookmark and reference https://www.w3schools.com/cssref/ if they can't get a property to work.

## 17. Instructor Demo: Using CSS Selectors (5 min)

It's important for students to understand how to use CSS selectors to target elements on a page. Understanding how to target the base HTML elements of our page is fundamental in understanding CSS selectors.

The previous example was targeting an anchor tag, so show students how to write selectors for other HTML elements on your page.

- Open Fav_Musician.html from the My Favorite Musician Activity example.
  [Fav_Musician.html Location](Activities/My_Favorite_Musician/Fav_Musician.html)

- Using Visual Studio Code create a folder named css in the My_Favorite_Musician folder.

- Create a file named style.css inside your newly created CSS folder.

- Add a link to your style.css file that you just created.

  ```html
  <link href="css/style.css" rel="stylesheet" type="text/css">
  ```

- Write a CSS selector that colors your H1 element red. ( h1 {color:red;} )

- Write a CSS selector that colors all your list item element blue. ( li {color:blue;} )

- Write a CSS selector that colors the body of HTML of your webpage yellow. ( html {background-color:yellow;} )

Students now know how HTML and CSS files work together and how to link them, but they must apply these concepts to truly grasp the concept.

## 18. Student Do: Add Some Color (15 min)

Now that we know how to add CSS to our site and the correct syntax, let's try out our new knowledge.

Ask TAs to Slack out the activity file: [16-Week/02-Activities/16.1/16.1-04-Add Some Color Activity Instructions](https://docs.google.com/document/d/1rRl5MwZ_xUvhoLEvbGo_8cSiK_5tcpnkUNoydJP1lGo/edit).

- Tell students to open Fav_Musician.html (the file they created earlier).

- Tell students it's their turn to add some color to the stylesheet.

- Make sure you leave your example CSS up on the projector for students to reference.

- Walk around and help students who are struggling. Have your co-instructor and TAs do the same.

DOCUMENTATION ON CSS SELECTORS: Have TAs Slack out: https://www.w3schools.com/css/css_syntax.asp. Read until the ID Selector section.

## 19. Everyone Do: CSS Review (5 min)

Let's take a step back and review what we have learned so far about CSS and empty elements.

- Ask the class the following questions to generate discussion. Feel free to get 2 or 3 answers per question if you have a lot of hands in the air.

- "What is an empty element? How does an empty element differ from a regular HTML element?"
- "How do I target the base HTML elements on a webpage?"
- "What does CSS stand for?"
- "Where does a link tag belong? What is a link tag used for?"

## 20. Instructor Do: Customize The Web (5 min)

Continue to lecture on the following CSS properties. Introduce the following topics to the class:

1. How to manipulate fonts with CSS: Have TAs Slack out the documentation: https://www.w3schools.com/css/css_font.asp.

- font-family - This property is used to control what fonts display on the web. There are three options that are passed to it. These serve as fallback fonts.
- font-style - This controls if a font displays as normal (which is default), italic, or oblique.
- font-size - This controls the pixel size of your fonts.
- font-weight - This controls the weight of the font. Options are from 100 (light) to 900 (black). Only values in intervals of 100 can be specified.

2. Height and width of elements. Also talk about how you can see elements that you are styling with the outline property!

- Height - Controls the pixel height of an element.
- Width - Controls the pixel width of an element. Width can also be specified as a percentage to tell the element how much of its container it should take up.
- Outline - Outlines literally create an outline of the html element, similar to borders, except they don't affect the box model.

## 21. Instructor Demo: Fonts on the Web (10 min)

Review adding google fonts.

- https://fonts.google.com/specimen/Roboto?selection.family=Roboto

## 22. Student Do: Style Your Favorite Musician (15 min)

Now it's the students' turn to practice what we just learned. Ask TAs to Slack out the activity file: [Style Your Musician Activity Instructions](https://docs.google.com/document/d/1o-1-F-6FU_a4XDBLhSLMpK3K7YCfZwOH5AX2cB8bydY/edit).

The goal of this exercise is for students to cement the fundamentals of what they learned during today's class. Students should also start to familiarize themselves with reading documentation.

- Reading documentation is crucial for anyone learning about programming.

Before this activity begins, tell students that Googling is normal and part of any developer's workflow and that they should practice their Google fu!

![For New Developers...](Images/googleLOL.png)

No more hints in the comments! It's time for students to be creative and style their favorite musician the way they want.

- Slack out relevant documentation about the properties we talked about during today's class.
- Encourage students to read the documentation if they have questions about certain properties.
- Be sure to walk around and talk to students. There will be lots of questions.

Relevant Documentation:

- CSS fonts and how to use them: https://www.w3schools.com/css/css_font.asp
- An overview of CSS syntax: https://www.w3schools.com/css/css_syntax.asp
- File paths and images. How to link to files in folders: https://www.w3schools.com/html/html_filepaths.asp
- Img tag Documentation: https://www.w3schools.com/tags/tag_img.asp
- List tag documentation: https://www.w3schools.com/html/html_lists.asp
- CSS dimensions: https://www.w3schools.com/css/css_dimension.asp

## 23. Everyone Do: Group Critique - Style Your Favorite Musician! (5 min)

- Ask students to share their work in the #Group_Critique channel on Slack.
- Pick 2 examples and pull them up on the display.
- Ask students what they like about it and if there is anything about the design that they would do differently (as opposed to "I don't like..., etc.").
- Encourage students to comment on each other's files in Slack, saying what they like or might do differently in the design.

## 24. Recap and End Class (1 min)

Review what we learned in class today. Take a moment and ask the class if they have any questions in regard to HTML, CSS or any of the programs we introduced today. Say good night and dismiss the class.

## 25. Introduce Homework 16 and End Class (5 min)

Ask TAs to Slack out the link to this week's homework:

 - [Unit 16 Homework Instructions](https://docs.google.com/document/d/1EXppZ1cPLCph70NuZFZEVk1B6ayXikUCbF6WkBauwk0/edit#heading=h.wodmsuj2fnfj)

This week’s homework is to create a very basic About Me webpage for yourself, style it with css, and upload it to Github to publish via Github Pages.

**Prompt:** You’re a new developer and probably wondering, “Where do I start?” All developers start with small, easy-to-digest projects. An About Me page is a good place to start to introduce yourself to the world wide web.

Students will:

- Create a Github repository and pull the blank repo to their computers.
- Create appropriate folder structure inside of the blank repo they just cloned to hold their working files. Project folder > images / css / index.html.
- Create the html file and structure of the wireframe.
- Style the wireframe using css to make their layout unique.
- Push their changes back to Github and publish to Github pages.

---

## We Want Your Feedback!

Please submit any issues or comments on the UX/UI curriculum to our Google Form.

[Submit Issues](https://docs.google.com/forms/d/e/1FAIpQLScTc104D7Fd-2fDk3E4IIwxuOe-BNhPhWffIE9VBt7_e-t3DA/viewform)

With this form you can now view the status of your submission and other issues here:
[Issue Status](https://docs.google.com/spreadsheets/d/1UyRh0f6fwtMD5SfExvk3BZxIIioicTNhXWixjmnes1c/edit?usp=sharing)

Also email mfoley@trilogyed.com and let us know how you felt today's class went.
Please include in the email subject your: University & Unit feedback e.g. UCLA - 16.1 Feedback. We'd love to hear what you think!

We will also be reaching out individually to check in and gather feedback about how your course is going.

---

## Copyright

© 2019 Trilogy Education Services, a 2U, Inc. brand. All Rights Reserved.
